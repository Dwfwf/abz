import { SizeEnum } from "../../enums/size.enum";

export const device = {
  mobileL: `(min-width: ${SizeEnum.mobile360})`,
  mobileH: `(max-width: ${SizeEnum.mobile360})`,
  tablet: `(min-width: ${SizeEnum.tablet768})`,
  laptop: `(min-width: ${SizeEnum.laptop1024})`,
  desktop: `(min-width: ${SizeEnum.desktop2560})`,
};
