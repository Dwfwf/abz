export enum ColorEnum {
  PrimaryColor = "#F4E041",
  SecondaryColor = "#00BDD3",
  LightGrayColor = "#f8f8f8",
  GrayColor = "#D0CFCF",
  GrayPrimary = "#7E7E7E",
  BlackColor = "#000000",
}
