export enum SizeEnum {
  mobile360 = "360px",
  tablet768 = "768px",
  laptop1024 = "1024px",
  desktop2560 = "2560px",
}
