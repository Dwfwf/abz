import styled from "styled-components";
import { device } from "../assets/responsive/responsive";
import { Typography } from "../typography/typography";

export const Wrapper = styled.div`
  @media ${device.mobileH} {
    display: flex;
    padding: 4px 8px;
    flex-direction: column;
  }
  @media ${device.mobileL} {
    display: flex;
    padding: 8px 16px;
    justify-content: space-between;
    flex-direction: row;
  }
  @media ${device.tablet} {
    padding: 8px 32px;
  }
  @media ${device.laptop} {
    padding: 8px 0;
  }
`;

export const ContainerButton = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media ${device.mobileL} {
    padding: 4px 8px;
    flex-direction: row;
  }
  @media ${device.mobileH} {
    padding: 8px 0 8px;
    justify-content: center;
  }
`;

export const ContainerText = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media ${device.mobileH} {
    flex-direction: row;
  }
  @media ${device.mobileH} {
    padding: 4px 8px;
    justify-content: center;
  }
`;
export const Mew = styled.img`
  width: 48px;
  height: 48px;
  object-fit: cover;
  border-radius: 50%;
`;

export const Text = styled(Typography)`
  color: black;
`;

export const ButtonWrapper = styled.div`
  margin-right: 8px;

  @media ${device.mobileL} {
    margin-right: 8px;
  }
  @media ${device.tablet} {
    margin-right: 16px;
  }
  @media ${device.laptop} {
    margin-right: 16px;
  }
`;
