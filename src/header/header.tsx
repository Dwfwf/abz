import {
  ButtonWrapper,
  ContainerButton,
  ContainerText,
  Text,
  Wrapper,
} from "./header.styles";
import Cat from "../assets/image/cat.svg";
import Image from "next/image";
import { ButtonCustom } from "../styles/button/button-custom";
import { Typography } from "../typography/typography";

export const Header = ({ allUsers, setAllUsers }: any) => {
  return (
    <Wrapper>
      <ContainerText>
        <Image src={Cat} width={50} height={50} alt="logo" />
        <Text type="nunitoMedium16">TESTTASK</Text>
      </ContainerText>
      <ContainerButton>
        <ButtonWrapper>
          <ButtonCustom
            variant="contained"
            disableRipple
            onClick={() => setAllUsers(true)}
          >
            <Typography type={"nunitoMedium16"}>User</Typography>
          </ButtonCustom>
        </ButtonWrapper>
        <ButtonCustom variant="contained" disableRipple>
          <Typography type={"nunitoMedium16"}>Sign in</Typography>
        </ButtonCustom>
      </ContainerButton>
    </Wrapper>
  );
};
