import { Button, InputAdornment, TextField } from "@mui/material";
import { useState } from "react";

import { styled } from "@mui/material/styles";
export const InputFile = () => {
  const [value, setValue] = useState("");

  const handleChange = (event: any) => setValue(event.target.value);

  const Input = styled("input")({
    display: "none",
  });

  return (
    <TextField
      type="file"
      value={value}
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            {/*<Icon type="Search" />*/}
          </InputAdornment>
        ),
      }}
      label={value.length > 0 ? "Upload your photo" : undefined}
      placeholder="Upload your photo"
      onChange={handleChange}
      fullWidth
    />
  );
};
