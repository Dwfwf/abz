import styled from "styled-components";
import { ColorEnum } from "../enums/color.enum";

export const Wrapper = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  height: 40px;
  border-radius: 4px;
  box-shadow: 0 0 1px 1px ${ColorEnum.GrayColor};
`;

export const IconWrapper = styled.div`
  display: flex;
  flex: 0 0 40px;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 100%;
`;

export const InputStyle = styled.input`
  flex: 1;
  background-color: transparent;
  border: none;
  outline: none;
`;
