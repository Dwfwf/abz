import { FC, InputHTMLAttributes } from "react";
import { useField } from "formik";
import { IconWrapper, InputStyle, Wrapper } from "./input.styles";

export interface InputProps extends InputHTMLAttributes<any> {
  icon?: any;
  name: string;
}

export const Input: FC<InputProps> = ({ name, icon: Icon, ...props }) => {
  const [field] = useField(name);

  return (
    <Wrapper>
      {Icon !== undefined && (
        <IconWrapper>
          <Icon />
        </IconWrapper>
      )}
      <InputStyle {...field} {...props} />
    </Wrapper>
  );
};
