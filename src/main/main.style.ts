import styled from "styled-components";

export const Wrapper = styled.div`
  max-width: 1070px;
  height: 100vh;
  margin: 0 auto;
`;
