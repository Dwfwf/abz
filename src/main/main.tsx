import { Header } from "../header/header";
import { Wrapper } from "./main.style";
import { Promo } from "../promo/promo";
import Users from "../users/users";
import React from "react";
import { Register } from "../register/register";

export const Main = ({
  users,
  positionsList,
  setShowUsers,
  showUsers,
  total,
  allUsers,
  setAllUsers,
}: any) => (
  <Wrapper>
    <Header allUsers={allUsers} setAllUsers={setAllUsers} />
    <Promo />
    <Users
      showUsers={showUsers}
      setShowUsers={setShowUsers}
      total={total}
      users={users}
      allUsers={allUsers}
    />
    <Register positionsList={positionsList} />
  </Wrapper>
);
