import Stack from "@mui/material/Stack";
import { ColorEnum } from "../enums/color.enum";
import CircularProgress from "@mui/material/CircularProgress";

export const Preloader = () => (
  <Stack sx={{ color: ColorEnum.SecondaryColor }} spacing={2} direction="row">
    <CircularProgress color="primary" />
  </Stack>
);
