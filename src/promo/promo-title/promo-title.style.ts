import { device } from "../../assets/responsive/responsive";
import styled from "styled-components";
import { Typography } from "../../typography/typography";
import { ColorEnum } from "../../enums/color.enum";
import { ButtonCustom } from "../../styles/button/button-custom";
import { Box } from "@mui/material";

export const Wrapper = styled.div`
  margin: 0 auto;
  transform: translateY(-440px);
  @media ${device.mobileL} {
    width: 328px;
    height: 120px;
  }
  @media ${device.mobileH} {
    transform: translateY(-475px);
  }
  @media ${device.tablet} {
    transform: translateY(-455px);
  }
  @media ${device.laptop} {
    transform: translateY(-455px);
  }
`;

export const TitleWrapper = styled(Box)``;

export const ItemWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
`;
export const Title = styled(Typography)`
  @media ${device.mobileH} {
    padding: 16px 16px;
  }
  @media ${device.tablet} {
    padding: 20px 0 20px;
  }
  @media ${device.laptop} {
    padding: 20px 0 20px;
  }
  color: ${ColorEnum.LightGrayColor};
`;
export const Subtitle = styled(Typography)`
  @media ${device.mobileH} {
    padding: 8px 8px 24px;
  }
  @media ${device.mobileL} {
    padding: 24px 0 24px;
  }

  color: ${ColorEnum.LightGrayColor};
`;

export const ButtonWrapper = styled(ButtonCustom)`
  width: 100px;
  display: flex;
  align-self: center;
  margin-top: 16px;
`;
