import {
  ButtonWrapper,
  ItemWrapper,
  Subtitle,
  Title,
  Wrapper,
} from "./promo-title.style";
import { Typography } from "../../typography/typography";
import { mockTitle } from "../../mock/title.mock";

interface mockTitleInterface {
  id: string;
  title: string;
  subtitle: string;
}

const PromoTitle = () => (
  <Wrapper>
    {mockTitle.map(({ id, title, subtitle }: mockTitleInterface) => (
      <ItemWrapper key={id}>
        <Title type="nunitoMedium40">{title}</Title>
        <Subtitle type="nunitoMedium16">{subtitle}</Subtitle>
        <ButtonWrapper>
          <Typography type="nunitoMedium16">Sign up</Typography>
        </ButtonWrapper>
      </ItemWrapper>
    ))}
  </Wrapper>
);

export default PromoTitle;
