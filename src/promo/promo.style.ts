import { Box } from "@mui/material";
import styled from "styled-components";

export const ImageWrapper = styled(Box)`
  overflow: hidden;
  position: relative;
  width: 100%;
  height: 500px;
`;

export const Wrapper = styled.div``;
