import { ImageWrapper, Wrapper } from "./promo.style";
import Imag from "../assets/image/background.png";
import Images from "next/image";
import PromoTitle from "./promo-title/promo-title";
import { Title } from "../title/title";

export const Promo = () => {
  return (
    <Wrapper>
      <ImageWrapper>
        <Images
          src={Imag}
          alt=""
          layout="fill"
          objectFit="cover"
          placeholder="blur"
          priority
        />
      </ImageWrapper>
      <PromoTitle />
      <Title>Working with GET request</Title>
    </Wrapper>
  );
};
