import React from "react";
import { Grid } from "@mui/material";
import { ButtonCustom } from "../../styles/button/button-custom";
import { Typography } from "../../typography/typography";

const RegisterButton = ({ onClick }: any) => {
  return (
    <Grid container justifyContent="center" pt={6.25} pb={12.5}>
      <ButtonCustom type={"submit"} onClick={onClick} variant="contained">
        <Typography type={"nunitoMedium16"}>Sign Up</Typography>
      </ButtonCustom>
    </Grid>
  );
};

export default RegisterButton;
