import React from "react";
import { Form, useField, useFormikContext } from "formik";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Grid,
  Radio,
} from "@mui/material";
import RadioGroup from "@mui/material/RadioGroup";
import { Typography } from "../../../typography/typography";
import { ColorEnum } from "../../../enums/color.enum";
import { Box } from "@mui/system";

export const RegisterCheckbox = ({
  handleChange,
  positionsList,
  ...otherProps
}: any) => {
  return (
    <Grid container py={1}>
      <FormControl>
        <FormLabel id="demo-radio-buttons-group-label">
          <Box sx={{ color: ColorEnum.BlackColor }}>Select your position</Box>
        </FormLabel>
        <RadioGroup
          aria-labelledby="demo-radio-buttons-group-label"
          defaultValue={"1"}
        >
          {positionsList.map(({ id, name }: any) => (
            <FormControlLabel
              value={id}
              name={"position_id"}
              onChange={handleChange}
              control={
                <Radio
                  sx={{
                    color: ColorEnum.GrayColor,
                    "&.Mui-checked": {
                      color: ColorEnum.SecondaryColor,
                    },
                  }}
                  color="secondary"
                />
              }
              label={name}
            />
          ))}
        </RadioGroup>
      </FormControl>
    </Grid>
  );
};

export default RegisterCheckbox;
