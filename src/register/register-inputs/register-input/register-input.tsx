import React from "react";
import { Grid } from "@mui/material";
import { TextFieldWrapper } from "../../../input/text-field-wrapper";
import { Box } from "@mui/system";
import { ColorEnum } from "../../../enums/color.enum";
import { Typography } from "../../../typography/typography";

const RegisterInput = () => {
  return (
    <>
      <Grid item py={3}>
        <TextFieldWrapper label={"Your name"} name={"name"}>
          Your name
        </TextFieldWrapper>
      </Grid>
      <Grid item py={3}>
        <TextFieldWrapper label={"Email"} name={"email"}>
          Email
        </TextFieldWrapper>
      </Grid>
      <Grid item py={3}>
        <TextFieldWrapper label={"Phone"} name={"phone"}>
          Phone
        </TextFieldWrapper>
        <Box sx={{ color: ColorEnum.GrayColor }} pt={0.5}>
          <Typography type="nunitoMedium12">+38 (XXX) XXX - XX - XX</Typography>
        </Box>
      </Grid>
    </>
  );
};

export default RegisterInput;
