import React, {useState} from "react";
import {Grid,} from "@mui/material";
import {Form, Formik} from "formik";
import * as Yup from "yup";
import RegisterInput from "./register-input/register-input";
import RegisterCheckbox from "./register-checkbox/register-checkbox";
import RegisterPhoto from "../register-photo/register-photo";
import RegisterButton from "../register-button/register-button";
import axios from "axios";

interface FormValues {
  name: string;
  email: string;
  phone: string;
  position_id: string;
  photo: string;
}

const INITIAL_FROM_STATE: FormValues = {
  name: "",
  email: "",
  phone: "",
  position_id: "1",
  photo: "",
};
const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const FORM_VALIDATION = Yup.object().shape({
  name: Yup.string().required("Required"),
  email: Yup.string().email("Invalid email.").required("Required"),
  phone: Yup.string().matches(phoneRegExp, "Phone number is not valid"),
  position_id: Yup.string(),
  photo: Yup.string(),
  //   .oneOf([true], "Выберите профессию")
  //   .required("The terms and conditions must be "),
});
const RegisterInputs = ({ positionsList }: any) => {
  const [data, setData] = useState<any>(INITIAL_FROM_STATE);

  // const uploadPhoto = (event: any, value) => {
  //   setData((prevState: any) => ({
  //     ...prevState,
  //     photo: event.target,
  //   }));
  // };

  const uploadHandler = async (value: any) => {
    const formData = new FormData();
    formData?.append("position_id", value.position_id);
    formData?.append("name", value.name);
    formData?.append("email", value.email);
    formData?.append("phone", value.phone);
    formData?.append("photo", value.photo);
    let token = "";
    const getToken = async () => {
      await axios
        .get("https://frontend-test-assignment-api.abz.agency/api/v1/token")
        .then((res) => {
          token = res.data;
        })
        .catch((error) => {
          console.error(error);
        });
    };
  };

  //   await axios({
  //     method: "post",
  //     url: "https://frontend-test-assignment-api.abz.agency/api/v1/users",
  //     data: formData,
  //     headers: { token, getToken },
  //   })
  //     .then(function (response) {
  //       //handle success
  //       console.log(response);
  //     })
  //     .catch(function (response) {
  //       //handle error
  //       console.log(response);
  //     });
  // };

  return (
    <Grid container px={2} py={1} justifyContent={"center"} xs={12}>
      <Formik
        initialValues={{ ...INITIAL_FROM_STATE }}
        validationSchema={FORM_VALIDATION}
        onSubmit={(values: any) => {
          values;
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          isValid,
          handleSubmit,
        }) => (
          <Form>
            <pre>{JSON.stringify(values, null, 2)}</pre>
            <RegisterInput />
            <RegisterCheckbox
              handleChange={handleChange}
              positionsList={positionsList}
            />
            <RegisterPhoto handleChange={handleChange} />
            <RegisterButton
              onClick={() => console.log("ey")}
              values={values}
              uploadHandler={uploadHandler}
            />
          </Form>
        )}
      </Formik>
    </Grid>
  );
};

export default RegisterInputs;
