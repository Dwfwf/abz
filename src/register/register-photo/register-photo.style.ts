import styled from "styled-components";
import { Box, Button } from "@mui/material";
import { ColorEnum } from "../../enums/color.enum";

export const Wrapper = styled.div`
  custom-file-button {
    input[type="file"] {
      margin-left: -2px !important;
    }
  }
  -webkit-file-upload-button {
    display: none;
  }
  file-selector-button {
    display: none;
  }
  &:hover {
    label {
      background-color: #dde0e3;
      cursor: pointer;
    }
  }
`;
export const PlaceholderWrapper = styled.input`
  position: relative;
  cursor: pointer;
  width: 400px;
  height: 40px;
  color: #f8f8f8;
`;

export const SpanWrap = styled.span`
  display: block;
  float: left;
  width: 20%;
  background-color: #42b5e8;
  color: #fff;
  height: 40px;
  line-height: 40px;
  padding: 0 10px;
`;

export const WrapperItem = styled.div`
  width: 100%;
  height: 40px;
  border: 1px solid #ccc;
`;

export const WrapperText = styled.span`
  display: block;
  float: left;
  line-height: 40px;
  width: 80%;
  padding-left: 10px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

export const ButtonItem = styled(Button)`
  text-transform: capitalize;
  color: ${ColorEnum.BlackColor};
  border-color: ${ColorEnum.BlackColor};
  border-radius: 5px 0 0 5px;
  &:hover {
    color: ${ColorEnum.BlackColor};
    border-color: ${ColorEnum.BlackColor};
  }
`;

export const InputItem = styled(Box)`
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1;
  min-width: 280px;
  height: 40px;
  color: ${ColorEnum.GrayColor};
  border-radius: 0 5px 5px 0;
  box-shadow: 0 0 1px 1px ${ColorEnum.GrayPrimary};
  &:hover {
    cursor: pointer;
  }
`;
