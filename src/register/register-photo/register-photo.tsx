import React, { useState } from "react";

import { Input } from "../../input/input";
import { Grid, Stack } from "@mui/material";
import { styled } from "@mui/material/styles";
import { ButtonItem, InputItem } from "./register-photo.style";
import Box from "@mui/material/Box";
import { Typography } from "../../typography/typography";

const RegisterPhoto = ({ handleChange }: any) => {
  // const fileUploadHandler = () => {
  //   const fromData = new FormData();
  //   // file from input type='file'
  //   // var fileField = document.querySelector('input[type="file"]');
  //   // formData.append('position_id', 2);
  //   // formData.append('name', 'Jhon');
  //   // formData.append('email', 'Jhon@gmail.com');
  //   // formData.append('phone', '+380955388485');
  //   // formData.append('photo', fileField.files[0])
  // };
  const [fileName, setFileName] = useState("Upload Boundary File");

  const Input = styled("input")({
    display: "none",
  });

  return (
    <Grid container pt={5.87}>
      <Stack direction="row" alignItems="center" spacing={2}>
        <label htmlFor="photo">
          <Input
            // accept="image/*"
            // id="photo"
            // multiple
            type="file"
            onChange={handleChange}
          />
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Box>
              <ButtonItem variant="outlined" size="large" >
                Upload
              </ButtonItem>
            </Box>
            <InputItem>
              <Typography type={"nunitoMedium16"}>Upload your photo</Typography>
            </InputItem>
          </Box>
        </label>
      </Stack>
    </Grid>
  );
};

export default RegisterPhoto;
