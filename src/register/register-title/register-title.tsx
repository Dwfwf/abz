import React from "react";
import { Title } from "../../title/title";
import { Box } from "@mui/material";

const RegisterTitle = () => {
  return (
    <Box pt={10} pb={2}>
      <Title>Working with POST request</Title>
    </Box>
  );
};

export default RegisterTitle;
