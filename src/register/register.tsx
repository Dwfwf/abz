import RegisterTitle from "./register-title/register-title";
import RegisterInputs from "./register-inputs/register-inputs";

export function Register({ positionsList }: any) {
  return (
    <>
      <RegisterTitle />
      <RegisterInputs positionsList={positionsList} />
    </>
  );
}
