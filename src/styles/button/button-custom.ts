import { styled } from "@mui/material/styles";
import Button from "@mui/material/Button";
import { ColorEnum } from "../../enums/color.enum";

export const ButtonCustom = styled(Button)({
  boxShadow: "none",
  textTransform: "none",
  minWidth: "100px",
  minHight: "30px",
  fontSize: 16,
  padding: "6px 12px",
  border: "1px solid",
  lineHeight: "26px",
  color: "black",
  fontWeight: "400",
  borderRadius: "80px",
  backgroundColor: `${ColorEnum.PrimaryColor}`,
  borderColor: `${ColorEnum.PrimaryColor}`,
  fontFamily: [
    "-apple-system",
    "BlinkMacSystemFont",
    '"Segoe UI"',
    "Roboto",
    '"Helvetica Neue"',
    "Arial",
    "sans-serif",
    '"Apple Color Emoji"',
    '"Segoe UI Emoji"',
    '"Segoe UI Symbol"',
    '"Nunito"',
  ].join(","),
  "&:hover": {
    backgroundColor: `${ColorEnum.PrimaryColor}`,
    borderColor: `${ColorEnum.PrimaryColor}`,
    boxShadow: "none",
  },
  "&:active": {
    boxShadow: "none",
    backgroundColor: `${ColorEnum.PrimaryColor}`,
    borderColor: `${ColorEnum.PrimaryColor}`,
  },
  "&:focus": {
    boxShadow: "0 0 0 0.2rem rgba(0,123,255,.5)",
  },
});
