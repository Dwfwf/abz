export const FontThemeStyle = {
  nunitoMedium40: {
    fontFamily: "Nunito",
    fontWeight: 400,
    fontSize: 40,
    lineHeight: "40px",
  },
  nunitoMedium16: {
    fontFamily: "Nunito",
    fontWeight: 400,
    fontSize: 16,
    lineHeight: "26px",
  },
  nunitoMedium12: {
    fontFamily: "Nunito",
    fontWeight: 400,
    fontSize: 12,
    lineHeight: "14px",
  },
};
