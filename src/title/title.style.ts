import styled from "styled-components";
import { Box } from "@mui/system";

export const Wrapper = styled(Box)`
  display: flex;
  justify-content: center;
  width: auto;
  align-items: center;
`;
