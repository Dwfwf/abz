import React from "react";
import { Box } from "@mui/system";
import { Typography } from "../typography/typography";
import { Wrapper } from "./title.style";
interface TitleInterface {
  children: string;
}

export const Title = ({ children }: TitleInterface) => (
  <Wrapper>
    <Box sx={{ textAlign: "center" }}>
      <Typography type="nunitoMedium40">{children}</Typography>
    </Box>
  </Wrapper>
);
