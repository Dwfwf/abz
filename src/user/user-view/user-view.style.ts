import styled from "styled-components";
import { Box, Grid } from "@mui/material";
import { ColorEnum } from "../../enums/color.enum";
import { Typography } from "../../typography/typography";

export const UserAvatar = styled(Box)`
  overflow: hidden;
  position: relative;
  width: 100%;
  height: 500px;
`;

export const Wrapper = styled.div``;

export const Card = styled(Grid)`
  display: flex;
  justify-content: center;
  margin: 20px;
  width: 250px;
  background-color: #ffffff;
  color: ${ColorEnum.LightGrayColor};
  border: 1px solid ${ColorEnum.LightGrayColor};
  border-radius: 10px;
  transition: color 0.15s ease, border-color 0.15s ease;
  background-color: white;
  &:focus {
    color: ${ColorEnum.PrimaryColor};
    border-color: ${ColorEnum.PrimaryColor};
  }
  &:before {
    color: ${ColorEnum.LightGrayColor};
    border-color: ${ColorEnum.LightGrayColor};
  }
`;
export const WrapperItems = styled.div`
  display: flex;
  color: black;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
export const Avatar = styled.img`
  display: inline-block;
  border-radius: 50%;
  max-width: 75px;
  max-height: 75px;
`;

export const Name = styled(Typography)`
  display: inline-block;
  padding: 10px 0;
`;
export const Position = styled(Typography)``;
export const Email = styled(Typography)``;
export const Phone = styled(Typography)``;
