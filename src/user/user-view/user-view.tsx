import {
  Avatar,
  Card,
  Email,
  Name,
  Phone,
  Position,
  WrapperItems,
} from "./user-view.style";
import { Grid } from "@mui/material";

export const UserView = ({
  user: { name, email, position, phone, photo },
}: any) => {
  return (
    <Card item py={2}>
      <WrapperItems>
        <Avatar src={photo} alt={""} />
        <Name type={"nunitoMedium16"}>{name}</Name>
        <Position type={"nunitoMedium16"}>{position}</Position>
        <Email type={"nunitoMedium16"}>{email}</Email>
        <Phone type={"nunitoMedium16"}>{phone}</Phone>
      </WrapperItems>
    </Card>
  );
};
