import styled from "styled-components";
import { Box } from "@mui/material";

export const WrapperItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
`;

// export const WrapperGrid = styled(Box)`
//   display: flex;
//   align-items: center;
//   justify-content: center;
//   flex-wrap: wrap;
//   max-width: 100%;
// `;
