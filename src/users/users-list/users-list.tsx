import { UsersListInteface } from "./users-list.inteface";
import { UserView } from "../../user/user-view/user-view";
import { Grid } from "@mui/material";

export function UsersList({ users }: any) {
  return (
    <Grid container justifyContent={"center"}>
      {users.map((user: UsersListInteface) => (
        <UserView key={user.id} user={user} />
      ))}
    </Grid>
  );
}
