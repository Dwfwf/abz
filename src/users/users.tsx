import { UsersList } from "./users-list/users-list";
import { Typography } from "../typography/typography";
import { ButtonCustom } from "../styles/button/button-custom";
import { Grid } from "@mui/material";
import { useEffect, useState } from "react";
import axios from "axios";
import { any } from "prop-types";
import Stack from "@mui/material/Stack";
import CircularProgress from "@mui/material/CircularProgress";
import { ColorEnum } from "../enums/color.enum";
import { Preloader } from "../preloader/preloader";

export default function Users({
  showUsers,
  setShowUsers,
  total,
  users,
  allUsers,
}: any) {
  return (
    <>
      {showUsers ? (
        <Grid container justifyContent="center" py={10}>
          <Preloader />
        </Grid>
      ) : (
        <UsersList users={users} />
      )}
      <Grid container justifyContent={"center"} py={2}>
        {typeof total === "object" ||
          (!allUsers && (
            <ButtonCustom
              disabled={showUsers}
              onClick={() => {
                return setShowUsers(true);
              }}
            >
              <Typography type={"nunitoMedium16"}>Show More</Typography>
            </ButtonCustom>
          ))}
      </Grid>
    </>
  );
}
